import {DOCUMENT_SETTINGS_PUT, DOCUMENT_SETTINGS_REMOVE} from "./action-types";

/*===================================================== Exports  =====================================================*/

export {
  putDocumentSettings,
  removeDocumentSettings
};

/*==================================================== Functions  ====================================================*/

function putDocumentSettings(documentId, settings) {
  return {
    type: DOCUMENT_SETTINGS_PUT,
    payload: {documentId, settings},
  };
}

function removeDocumentSettings(documentId) {
  return {
    type: DOCUMENT_SETTINGS_REMOVE,
    payload: {documentId},
  };
}
