import {Router} from "preact-router";

import PageLanding from "./routes/landing";
import PageDocument from "./routes/document";

/*===================================================== Exports  =====================================================*/

export default MyRouter;

/*==================================================== Component  ====================================================*/

function MyRouter() {
  return (
      <Router>
        <PageLanding path="/"/>
        <div path="/documents">Not implemented yet.</div>
        <PageDocument path="/document" id={null}/>
        <PageDocument path="/document/_/:id"/>
        <div default>404 Does not exist.</div>
      </Router>
  );
}
