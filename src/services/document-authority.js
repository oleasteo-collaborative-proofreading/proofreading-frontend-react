import EventEmitter from "./event-emitter";
import {Node} from "prosemirror-model";
import {Step} from "prosemirror-transform";
import schema from "../constants/document-schema";
import {BiStep} from "./prosemirror-rebase";
import {nextId} from "./id-scope";

const stepFromJSON = Step.fromJSON.bind(null, schema);

/*===================================================== Exports  =====================================================*/

export {
  parseRemoteState, rebaseRemoteState,
};

export default class DocumentAuthority extends EventEmitter {

  constructor(schema, connectors) {
    super();
    this._idCache = new Set();
    this._connectors = connectors;
    this._ready = [];
    this._readyIndex = -1;
    this._schema = schema;

    this.ready = this._start();
  }

  nextId(idScope = this.getStateNow().idScope) { return nextId(this._idCache, idScope); }

  async _start() {
    for (let i = 0; i < this._connectors.length; i++) {
      await (this._ready[i] = this._connectors[i].start(this));
      this._readyIndex = i;
    }
    this.emit("ready");
  }

  async getState(exclude) { // see parseRemoteState for expected values
    for (let i = 0; i < this._connectors.length; i++) {
      await this._ready[i];
      const connector = this._connectors[i];
      if (connector === exclude) { break; }
      const state = connector.state;
      if (state != null) { return state; }
    }
    return void 0;
  }

  getStateNow(exclude) {
    for (let i = 0; i <= this._readyIndex; i++) {
      const connector = this._connectors[i];
      if (connector === exclude) { break; }
      const state = connector.state;
      if (state != null) { return state; }
    }
    return void 0;
  }

}

/*==================================================== Functions  ====================================================*/

function parseRemoteState({doc: docObj, version, inverseSteps, idScope, comments, suggestions, chat}) {
  return {
    doc: Node.fromJSON(schema, docObj),
    version,
    inverseSteps: inverseSteps.map(stepFromJSON),
    idScope,
    comments,
    suggestions,
    chat: chat || [],
  };
}

function rebaseRemoteState({doc, version, inverseSteps}, makeBidirectional) {
  const steps = new Array(inverseSteps.length);
  const _last = inverseSteps.length - 1;
  for (let i = _last; i >= 0; i--) {
    const inverseStep = inverseSteps[i];
    steps[i] = makeBidirectional ? BiStep.fromInverseStep(inverseStep, doc) : inverseStep.invert(doc);
    // eslint-disable-next-line prefer-reflect
    doc = inverseStep.apply(doc).doc;
  }
  return {baseDoc: doc, baseVersion: version - steps.length, [makeBidirectional ? "biSteps" : "steps"]: steps};
}
