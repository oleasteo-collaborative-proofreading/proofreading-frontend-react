import {removeFromList} from "../utils/list";

/*===================================================== Exports  =====================================================*/

export default class EventEmitter {

  constructor() {
    this.listeners = {};
    this.listenersOnce = {};
    this.reducers = {};
  }

  once(type, listener) {
    if (!this.listenersOnce.hasOwnProperty(type)) { this.listenersOnce[type] = []; }
    const listeners = this.listenersOnce[type];
    listeners.push(listener);
    return removeFromList.bind(null, listeners, listener);
  }

  on(type, listener) {
    if (!this.listeners.hasOwnProperty(type)) { this.listeners[type] = []; }
    const listeners = this.listeners[type];
    listeners.push(listener);
    return removeFromList.bind(null, listeners, listener);
  }

  off(type, listener) {
    const listeners = this.listeners;
    const listenersOnce = this.listenersOnce;
    let removed = false;
    if (listeners.hasOwnProperty(type)) {
      removed = removeFromList(listeners[type], listener) || removed;
    }
    if (listenersOnce.hasOwnProperty(type)) {
      removed = removeFromList(listenersOnce[type], listener) || removed;
      if (listenersOnce[type].length === 0) { Reflect.deleteProperty(listenersOnce, type); }
    }
    return removed;
  }

  emit(type, payload) {
    if (this.listenersOnce.hasOwnProperty(type)) {
      for (let listener of this.listenersOnce[type]) { listener(payload, type); }
      Reflect.deleteProperty(this.listenersOnce, type);
    }
    if (this.listeners.hasOwnProperty(type)) {
      for (let listener of this.listeners[type]) { listener(payload, type); }
    }
    return this;
  }

  filter(type, reducer, priority = 0) {
    if (!this.reducers.hasOwnProperty(type)) { this.reducers[type] = []; }
    const reducers = this.reducers[type];
    const item = {priority, reducer};
    reducers.push(item);
    reducers.sort((r0, r1) => r1.priority - r0.priority);
    return removeFromList.bind(null, reducers, item);
  }

  async reduce(type, data) {
    let reducedData = data;
    if (this.reducers.hasOwnProperty(type)) {
      for (let item of this.reducers[type]) {
        const result = await item.reducer(reducedData, data);
        if (result === false) { return result; }
        if (result !== void 0) { reducedData = result; }
      }
    }
    return data;
  }

}
