import {parseRemoteState} from "../document-authority";

/*===================================================== Exports  =====================================================*/

export default class LocalStorageConnector { // TODO use IndexedDB instead of Web Storage

  constructor(documentId) {
    this._authority = null;
    this._documentId = documentId;
    this._storageKey = `DOCUMENT#${this._documentId}`;
    this.state = null;
  }

  async start(authority) {
    this._authority = authority;
    this.fetch();
    authority.on("state:update", this._updateState);
  }

  _updateState = (data) => {
    if (this.state === null) { return; } // todo on/off on en/disable instead
    this.state = {
      ...this._authority.getStateNow(),
      ...data,
    };
    this.persist();
  };

  fetch() {
    const value = localStorage.getItem(this._storageKey);
    if (value === null) { return; }
    this.state = parseRemoteState(JSON.parse(value));
  }

  disable() {
    this.state = null;
    localStorage.removeItem(this._storageKey);
  }

  async enable() {
    this.state = await this._authority.getState();
    this.persist();
  }

  persist() { localStorage.setItem(this._storageKey, JSON.stringify(this.state)); }

}
