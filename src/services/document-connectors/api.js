import {Step} from "prosemirror-transform";
import getSession from "../../api";
import {parseRemoteState, rebaseRemoteState} from "../document-authority";

const API_KEY_PUSH_STEPS = "document:steps";
const API_KEY_RECEIVE_STEPS = "document:steps";
const API_KEY_LISTEN = "document:subscribe"; // todo make subscribe + fetch an atomic operation
const API_KEY_LISTEN_STOP = "document:unsubscribe";

/*===================================================== Exports  =====================================================*/

export default class APIConnector {

  constructor(documentId) {
    this._api = getSession();
    this._authority = null;
    this._documentId = documentId;
    this._enabled = true;
    this.state = null;
  }

  async start(authority) {
    this._authority = authority;
    if (this._enabled) {
      this._startListen();
      await this.fetch();
    }
    authority.filter("document:local:publish", this._publish);
    authority.on("state:update", this._updateState);
  }

  _startListen() {
    this._api.notify(API_KEY_LISTEN, {documentId: this._documentId});
    this._api.listen(API_KEY_RECEIVE_STEPS, this._receiveSteps);
    this.__removeOnDestroy = this._authority.once("destroy", this._stopListen);
  }

  _stopListen = () => {
    this._api.notify(API_KEY_LISTEN_STOP, {documentId: this._documentId});
    this._api.mute(API_KEY_RECEIVE_STEPS, this._receiveSteps);
  };

  disable() {
    this.state = null;
    this._enabled = false;
    if (typeof this.__removeOnDestroy === "function") {
      this.__removeOnDestroy();
      this._stopListen();
    }
  }

  async enable() {
    this._enabled = true;
    this._startListen();
    await this.fetch();
  }

  async fetch() {
    const documentId = this._documentId;
    const localState = this._authority.getStateNow();
    const payload = localState == null ? {documentId} :
        {documentId, baseVersion: localState.version - localState.inverseSteps.length};
    this.state = parseRemoteState(await this._api.query("document:fetch", payload));
    if (localState != null) {
      const state = this.state;
      await this._authority.ready;
      if (this.state !== state) { return; }
      const {baseVersion, steps} = rebaseRemoteState(state, false);
      this._dispatchReceivedSteps({baseVersion, version: baseVersion + steps.length, steps});
    }
  }

  _publish = async ({baseVersion, biSteps}) => {
    if (this.state === null) { return false; }
    const transferData = {documentId: this._documentId, baseVersion, steps: biSteps.map(biStep => biStep.forward)};
    try {
      await this._api.query(API_KEY_PUSH_STEPS, transferData);
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error(error.message || error);
      return false;
    }
  };

  _updateState = (data) => {
    if (this.state === null) { return; } // todo on/off on en/disable instead
    this.state = {
      ...this.state,
      ...data,
    };
  };

  _receiveSteps = async ({documentId, baseVersion, steps: stepObjects}) => {
    if (documentId !== this._documentId) { return; }
    const schema = this._authority._schema;
    const steps = stepObjects.map((step) => Step.fromJSON(schema, step));
    await this._dispatchReceivedSteps({baseVersion, version: baseVersion + steps.length, steps});
  };

  _dispatchReceivedSteps(data) { this._authority.emit("document:foreign:update", data); }

}
