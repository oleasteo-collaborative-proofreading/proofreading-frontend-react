import {Component} from "preact";
import PropTypes from "prop-types";

/*==================================================== Component  ====================================================*/

class EditorControls extends Component {

  static propTypes = {
    active: PropTypes.any.isRequired,
    setActive: PropTypes.func.isRequired,
  };

  toggleSyncOnline = () => this.props.setActive({...this.props.active, syncOnline: !this.props.active.syncOnline});
  toggleSyncOffline = () => this.props.setActive({...this.props.active, syncOffline: !this.props.active.syncOffline});

  render({className, active: {syncOnline, syncOffline}, ...others}) {
    const classNames = ["action-list", className || ""].join(" ");
    const noSync = !syncOnline && !syncOffline;
    const syncOfflineClassNames = [
      "btn btn-link action-list-item tooltip", syncOffline ? "active" : "", noSync ? "text-error" : "",
    ].join(" ");
    const syncOnlineClassNames = [
      "btn btn-link action-list-item tooltip", syncOnline ? "active" : "", noSync ? "text-error" : "",
    ].join(" ");
    return (
        <div className={classNames} {...others}>
          <div className={"action-list-group"}>
            <button type={"button"} className={syncOfflineClassNames} onClick={this.toggleSyncOffline}
                    data-tooltip={"Toggle offline synchronization\n(save within local storage)"}>
              <i className="fas fa-fw fa-hdd"></i>
            </button>
            <button type={"button"} className={syncOnlineClassNames} onClick={this.toggleSyncOnline}
                    data-tooltip={"Toggle online synchronization\n(send/receive changes)"}>
              <i className="fas fa-fw fa-cloud"></i>
            </button>
            <div className="divider-vert"></div>
            <button type={"button"} className="btn btn-link action-list-item tooltip"
                    data-tooltip={"Modify document settings"}>
              <i className="fas fa-fw fa-wrench"></i>
            </button>
          </div>
          <div className={"action-list-group"}>
            <button type={"button"} className="btn btn-link action-list-item tooltip" disabled
                    data-tooltip={"Manage user access"}>
              <i className="fas fa-fw fa-users-cog"></i>
            </button>
            <button type={"button"} className="btn btn-link action-list-item tooltip"
                    data-tooltip={"Invite someone to access this document"}>
              <i className="fas fa-fw fa-user-plus"></i>
            </button>
            <div className="divider-vert"></div>
            <button type={"button"} className="btn btn-link action-list-item tooltip"
                    data-tooltip={"Show help"}>
              <i className="fas fa-fw fa-question"></i>
            </button>
          </div>
        </div>
    );
  }

}

export default EditorControls;
