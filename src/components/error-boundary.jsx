/* eslint-disable no-console */

import {Component} from "preact";
import PropTypes from "prop-types";

/*==================================================== Component  ====================================================*/

export default class ErrorBoundary extends Component {
  static propTypes = {
    children: PropTypes.any.isRequired,
  };

  state = {
    hasError: false,
    error: null,
    info: null,
  };

  componentDidCatch(error, info) {
    console.error(error, info);
    this.setState({hasError: true, error, info});
  }

  render() {
    if (this.state.hasError) {
      return (
          <div>
            <h1>Something went wrong</h1>
            <h2>{this.state.error.message}</h2>
            <pre>{this.state.info.componentStack}</pre>
            <pre>{this.state.error.stack}</pre>
          </div>
      );
    }

    return this.props.children[0]; // currently preact does not support array returns (#45)
  }
}
