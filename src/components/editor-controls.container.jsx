import {Component} from "preact";
import {connect} from "preact-redux";
import {putDocumentSettings} from "../store/action-creators";
import EditorControls from "./editor-controls";
import PropTypes from "prop-types";

/*==================================================== Container  ====================================================*/

@connect(mapStateToProps, mapDispatchToProps)
class EditorControlsContainer extends Component {

  static propTypes = {
    documentId: PropTypes.string.isRequired,
  };

  render(props) { return <EditorControls {...props}></EditorControls>; }

}

export default EditorControlsContainer;

/*==================================================== Functions  ====================================================*/

function mapStateToProps(state, {documentId}) {
  const settings = state.documentSettings[documentId];
  return {
    active: {
      syncOnline: settings == null ? true : settings.syncOnline,
      syncOffline: settings == null ? false : settings.syncOffline,
    },
  };
}

function mapDispatchToProps(dispatch, {documentId}) {
  return {
    setActive(settings) { dispatch(putDocumentSettings(documentId, settings)); },
  };
}
